/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     13.05.2015 21:34:43                          */
/*==============================================================*/


drop table if exists accounts;

drop table if exists accounts_estate_types;

drop table if exists estate_revisions;

drop table if exists estate_types;

drop table if exists estates;

drop table if exists users;

/*==============================================================*/
/* Table: accounts                                              */
/*==============================================================*/
create table accounts
(
   id                   int not null auto_increment,
   login                varchar(2000) not null,
   password             varchar(2000) not null,
   created_at           datetime not null,
   account_type         int not null,
   user_id              int not null,
   start_date           datetime,
   end_date             datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: accounts_estate_types                                 */
/*==============================================================*/
create table accounts_estate_types
(
   id                   int not null auto_increment,
   account_id           int not null,
   estate_type_id       int not null,
   primary key (id)
);

alter table accounts_estate_types comment 'Types of estate which is available for account';

/*==============================================================*/
/* Table: estate_revisions                                      */
/*==============================================================*/
create table estate_revisions
(
   id                   int not null auto_increment,
   account_id           int not null,
   estate_id            int not null,
   created_at           datetime not null,
   revision_result      int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: estate_types                                          */
/*==============================================================*/
create table estate_types
(
   id                   int not null auto_increment,
   title                varchar(2000) not null,
   primary key (id)
);

/*==============================================================*/
/* Table: estates                                               */
/*==============================================================*/
create table estates
(
   id                   int not null auto_increment,
   owner_name           varchar(2000) not null,
   owner_phone          varchar(2000) not null,
   title                varchar(2000) not null,
   description          varchar(2000),
   estate_type_id       int not null,
   status               int,
   is_deleted           tinyint(1) not null default 0,
   primary key (id)
);

/*==============================================================*/
/* Table: users                                                 */
/*==============================================================*/
create table users
(
   id                   int not null auto_increment,
   first_name           varchar(2000) not null,
   last_name            varchar(2000) not null,
   patronym             varchar(2000),
   is_deleted           tinyint(1) not null default 0,
   primary key (id)
);

alter table accounts add constraint FK_Reference_1 foreign key (user_id)
      references users (id) on delete restrict on update restrict;

alter table accounts_estate_types add constraint FK_Reference_3 foreign key (estate_type_id)
      references estate_types (id) on delete restrict on update restrict;

alter table accounts_estate_types add constraint FK_Reference_4 foreign key (account_id)
      references accounts (id) on delete restrict on update restrict;

alter table estate_revisions add constraint FK_Reference_5 foreign key (estate_id)
      references estates (id) on delete restrict on update restrict;

alter table estate_revisions add constraint FK_Reference_6 foreign key (account_id)
      references accounts (id) on delete restrict on update restrict;

alter table estates add constraint FK_Reference_2 foreign key (estate_type_id)
      references estate_types (id) on delete restrict on update restrict;

